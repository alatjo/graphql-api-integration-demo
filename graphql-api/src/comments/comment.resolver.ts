import { CommentResolverFn } from "../types";
import { Comment, ResolversParentTypes } from "../generated/graphql";

const commentResolver: CommentResolverFn<
  Promise<Comment[]>,
  ResolversParentTypes["Customer"]
> = async ({ id, email }, _args, { dataSources }) => {
  if (!email) {
    console.warn("no email field set on Customer with id: ", id);
    return [];
  }
  return await dataSources.CommentAPI.getMessagesByUserId(email);
};

export default commentResolver;
