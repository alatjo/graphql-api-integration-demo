import { Comment } from "../generated/graphql";

export interface ICommentAPI {
  getMessagesByUserId(userId: string): Promise<Comment[]>;
}
