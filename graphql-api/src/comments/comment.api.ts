import fetch from "node-fetch";
import { Comment } from "../generated/graphql";
import BaseAPI from "../shared/base.api";
import { ICommentAPI } from "./comment.interface";

export default class CommentAPI extends BaseAPI implements ICommentAPI {
  async getMessagesByUserId(userId: string): Promise<Comment[]> {
    return await fetch(this.baseURL)
      .then(async (response) => {
        const res = await response.json();
        return res.filter((comment: Comment) => comment.userId === userId);
      })
      .catch((error) => console.error(error));
  }
}
