import {
  customerResolver,
  customerByIdResolver,
} from "../customer/customer.resolver";
import commentResolver from "../comments/comment.resolver";

export default {
  Query: {
    customers: customerResolver,
    customer: customerByIdResolver,
  },
  Customer: {
    comments: commentResolver,
  },
};
