import { ApolloServer } from "apollo-server-lambda";
import { DataSources } from "apollo-server-core/dist/requestPipeline";

import resolvers from "./resolvers";
import CustomerAPI from "../customer/customer.api";
import { APIs } from "../types";
import CommentAPI from "../comments/comment.api";

const typeDefs: string = require("../schema.graphql");

const server = new ApolloServer({
  typeDefs,
  resolvers,
  dataSources: (): DataSources<APIs> => ({
    CustomerAPI: new CustomerAPI(
      process.env.CUSTOMER_REST_API_HOST,
      process.env.CUSTOMER_REST_API_PORT,
      "customers"
    ),
    CommentAPI: new CommentAPI(
      process.env.COMMENT_REST_API_HOST,
      process.env.COMMENT_REST_API_PORT,
      "comments"
    ),
  }),
});

exports.graphqlHandler = server.createHandler();
