import { Customer } from "../generated/graphql";
import { CustomerResolverFn } from "../types";
import { customerByIdResolverArgs } from "./customer.interface";

const customerResolver: CustomerResolverFn<Promise<Customer[]>> = async (
  _,
  __,
  { dataSources }
) => {
  return await dataSources.CustomerAPI.getCustomers();
};

const customerByIdResolver: CustomerResolverFn<
  Promise<Customer>,
  null,
  customerByIdResolverArgs
> = async (_, { customerId }, { dataSources }) => {
  return await dataSources.CustomerAPI.getCustomerById(customerId);
};

export { customerResolver, customerByIdResolver };
