import fetch from "node-fetch";
import { ICustomerAPI } from "./customer.interface";
import { Customer } from "../generated/graphql";
import BaseAPI from "../shared/base.api";

export default class CustomerAPI extends BaseAPI implements ICustomerAPI {
  async getCustomers(): Promise<Customer[]> {
    return await fetch(this.baseURL)
      .then((response) => response.json())
      .catch((error) => console.error(error));
  }
  async getCustomerById(customerId: number): Promise<Customer> {
    return await fetch(`${this.baseURL}/${customerId}`)
      .then((response) => response.json())
      .catch((error) => console.error(error));
  }
}
