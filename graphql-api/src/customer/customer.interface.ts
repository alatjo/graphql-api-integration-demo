import { Customer } from "../generated/graphql";

export interface ICustomerAPI {
  getCustomers(): Promise<Customer[]>;
}

export interface customerByIdResolverArgs {
  customerId: number;
}
