import { RESTDataSource } from "apollo-datasource-rest";

export default class BaseAPI extends RESTDataSource {
  private readonly url: string;

  constructor(host?: string, port?: string, path?: string) {
    super();
    this.url = `http://${host}:${port}/${path}`;
  }

  public get baseURL(): string {
    return this.url;
  }
}
