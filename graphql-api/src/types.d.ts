import CustomerAPI from "./customer/customer.api";
import { ResolverFn as GeneratedResolverFn } from "./generated/graphql";
import CommentAPI from "./comments/comment.api";

export interface APIs {
  CustomerAPI: CustomerAPI;
  CommentAPI: CommentAPI;
}

export interface ResolverContext {
  dataSources: APIs;
}

export type CustomerResolverFn<
  TResult,
  TParent = null,
  TArgs = null
> = GeneratedResolverFn<TResult, TParent, ResolverContext, TArgs>

export type CommentResolverFn<
  TResult,
  TParent,
  TArgs = null
> = GeneratedResolverFn<TResult, TParent, ResolverContext, TArgs>

