# GraphQL API integration demo

## Prerequisities
 - serverless (https://serverless.com/)
 - node v.12 (https://nodejs.org/en/)
 - yarn (https://yarnpkg.com/)

## Development
Start all services inside each folder:  

- `yarn install && yarn start`

### GraphQL API

- Playground: http://localhost:8080/graphql
- Test queries: *graphql-api/test/queries*

### Customer API

- endpoint: http://localhost:3000/customers
- mock data: *rest-customer-api/customer.json*

### Comments API

- endpoint: http://localhost:4000/comments
- mock data: *rest-comment-api/comments.json*

### Dashboard App

- UI: http://localhost:3001/
