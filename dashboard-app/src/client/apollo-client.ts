import ApolloClient from 'apollo-client'
import { createHttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'

const cache = new InMemoryCache()
const uri = process.env.REACT_APP_GRAPHQL_URI
const link = createHttpLink({ uri })

export default new ApolloClient({
  connectToDevTools: true,
  link,
  cache,
})
