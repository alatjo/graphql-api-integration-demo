import React from 'react'
import Header from './components/header/Header'
import Footer from './components/footer/Footer'
import Customer from './components/customer'

function App() {
  return (
    <>
      <Header />
      <Customer />
      <Footer />
    </>
  )
}

export default App
