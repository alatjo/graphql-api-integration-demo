import React from 'react'
import {
  Backdrop,
  Modal,
  makeStyles,
  Theme,
  createStyles,
  Fade,
  ListItem,
  ListItemAvatar,
  Avatar,
  ListItemText,
} from '@material-ui/core'
import MessageIcon from '@material-ui/icons/Message'
import ContactMailIcon from '@material-ui/icons/ContactMail'
import Typography from '@material-ui/core/Typography'
import { GetCustomerWithCommentsQuery } from '../../generated/graphql'

const useCustomerModalStyles = makeStyles((theme: Theme) =>
  createStyles({
    modal: {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'center',
    },
    modalContainer: {
      display: 'flex',
      flexDirection: 'column',
      backgroundColor: theme.palette.background.paper,
      opacity: 0.25,
      border: '2px solid #000',
      boxShadow: theme.shadows[5],
      padding: theme.spacing(2, 4, 3),
    },
    icon: {
      width: '100%',
      fontSize: '68px',
      margin: '1rem auto',
    },
    title: {
      margin: '1rem auto',
    },
  })
)

interface CustomerModalProps {
  data: GetCustomerWithCommentsQuery
  open: boolean
  handleClose: any
}

const CustomerModal = ({ data, open, handleClose }: CustomerModalProps) => {
  const classes = useCustomerModalStyles()
  const { customer } = data
  return (
    <Modal
      aria-labelledby="transition-modal-title"
      aria-describedby="transition-modal-description"
      className={classes.modal}
      open={open}
      onClose={handleClose}
      closeAfterTransition
      BackdropComponent={Backdrop}
      BackdropProps={{
        timeout: 500,
        invisible: false,
      }}
    >
      <Fade in={open}>
        <div className={classes.modalContainer}>
          <ContactMailIcon className={classes.icon} />
          <Typography className={classes.title} variant="h4" noWrap>
            {customer?.firstname} {customer?.lastname}
          </Typography>
          {customer?.comments?.map((comment, idx) => {
            return (
              <ListItem key={idx}>
                <ListItemAvatar>
                  <Avatar>
                    <MessageIcon />
                  </Avatar>
                </ListItemAvatar>
                <ListItemText primary={comment?.title} secondary={comment?.message} />
              </ListItem>
            )
          })}
        </div>
      </Fade>
    </Modal>
  )
}

export { CustomerModal }
