import React from 'react'
import { useGetCustomerWithCommentsQuery } from '../../generated/graphql'
import { CustomerModal } from './CustomerModal'

interface ModalProps { 
  id?: number,
  open: boolean,
  handleClose: any
}

export default function ({ id, open, handleClose }: ModalProps) {
  if(!id) return null

  const { data, loading, error } = useGetCustomerWithCommentsQuery({
    variables: {
      customerId: id,
    }
  })

  if (loading) return <p>loading...</p>
  if (!loading && error) return <p>Something went wrong...</p>
  if (data && data.customer) {
    return <CustomerModal data={data} open={open} handleClose={handleClose} />
  }
  return null
}
