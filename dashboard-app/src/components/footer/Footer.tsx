import React from 'react'
import { makeStyles } from '@material-ui/core'

const useFooterStyles = makeStyles(theme => ({
  footerContainer: {
    position: 'fixed',
    bottom: 0,
    width: '100%',
    background: theme.palette.primary.light,
  },
  text: {
    textAlign: 'right',
    marginRight: theme.spacing(2),
    color: theme.palette.primary.contrastText,
  },
}))


export default function Footer() {
  const styles = useFooterStyles()
  return (
    <div className={styles.footerContainer}>
      <p className={styles.text}>JoniA</p>
    </div>
  )
}