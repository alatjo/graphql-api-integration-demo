import React from 'react'
import { useGetCustomersQuery } from '../../generated/graphql'
import CustomerGrid from './CustomerGrid'
import CustomerGridSkeleton from './CustomerGridSkeleton'

export default function () {
  const { data, loading, error } = useGetCustomersQuery()  
  if (loading) return <CustomerGridSkeleton />
  if (!loading && error) return <p>Something went wrong...</p>
  if (data && data.customers && data.customers.length > 0) {
    return <CustomerGrid data={data} />
  }
  return null
}
