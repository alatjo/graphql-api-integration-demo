import React, { useState } from 'react'
import { makeStyles, createStyles, Theme } from '@material-ui/core/styles'
import { Grid, Paper, Zoom } from '@material-ui/core'
import { Flipper } from 'react-flip-toolkit'
import { GetCustomersQuery } from '../../generated/graphql'
import CustomerGridCard from './CustomerGridCard'
import CustomerModal from '../modal'

export const useCustomerGridStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1,
      marginTop: '3rem'
    },
    paper: {
      height: 240,
      width: 200,
      background: '#f6e1e1'
    },
  }),
)

interface CustomerGridProps {
  data: GetCustomersQuery
}

const CustomerGrid = ({ data }: CustomerGridProps) => {
  const classes = useCustomerGridStyles()
  const [open, setOpen] = useState(false)
  const [customerId, setSelectedId] = useState(undefined)

  const handleOpen = (id: any) => {
    setSelectedId(id)
    setOpen(true)
  }
  const handleClose = () => {
    setOpen(false)
  }

  return (
    <>
      <Flipper flipKey={data.customers?.map((customer) => customer?.id).join('')} spring="noWobble" className="flipper">
        <Grid container className={classes.root}>
          <Grid container justify="center" spacing={4}>
            {data.customers?.map((customer: any) => (
              <Zoom in timeout={500} key={customer.id}>
                <Grid key={customer.id} item onClick={() => handleOpen(customer.id)}>
                  <Paper className={classes.paper}>
                    <CustomerGridCard customer={customer} />
                  </Paper>
                </Grid>
              </Zoom>
            ))}
          </Grid>
        </Grid>
      </Flipper>
      <CustomerModal id={customerId} open={open} handleClose={handleClose} />
    </>
  )
}

export default CustomerGrid
